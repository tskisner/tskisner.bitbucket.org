
.. _install:

Installation
=======================

The python interface to SHDET is very minimal and consists of a wrapper around the highest level function (shdet_f).


Dependencies
--------------------

The underlying C code requires a compiler which supports the C11 standard.  The C code is compiled and bundled directly into the python module.

While the underlying C code has no external dependencies, in order to build the python interface you must have:

- python (obviously)
- setuptools
- numpy
- cython


Installing the Package and Test Program
-------------------------------------------------

You should NOT try to simply run the python code in place.  You should install a copy of the code to somewhere in your ``$PYTHONPATH``.  You do this with (for example)::

    $> python setup.py install --prefix=/home/user/software

That command will install the package to /home/user/software/lib/python2.7/site-packages, and this location must be in your ``$PYTHONPATH``.  For example, your shell resource file should contain something like::

    export PYTHONPATH=/home/user/software/lib/python2.7/site-packages:${PYTHONPATH}

It will also put any scripts (i.e. test_shdet.py) into /home/user/software/bin, which you should have in your ``$PATH``.


