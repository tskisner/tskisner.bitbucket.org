
.. _example:

Examples
===============

Here is a simple example which loads one of the C-style parameter files and simulates data using a constant sink temperature and zero optical power.  This is basically what is found in the included "test_shdet.py" script.::

    import numpy as np
    import shdet as shd

    nsamp = 100000

    # zero optical load
    p_optical = np.zeros([nsamp], dtype=np.float64)

    # constant 100mK sink temperature
    t_base = np.zeros([nsamp], dtype=np.float64)
    t_base.fill(0.01)

    # C-style parameter file
    params = shd.load_cparams('param/05_353_1/05_353_1_shdet_default.txt')

    # run the simulation
    bolo = shd.run(t_base, p_optical, params)

    # write to a text file
    np.savetxt('shdet_sim_out.txt', bolo, fmt='%.18e')

You can also do something more complicated.  Here we use mpi4py to simulate multiple rings of data from the same parameter file.  Only one process actually reads the parameter file.  All processes write the output rings of data to separate files.  This is basically what is found in the included "test_mpi_shdet.py" script.







